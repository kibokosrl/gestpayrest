<?php

use KibokoSrl\GestPayRest\Classes\Payment;

require './vendor/autoload.php';

$data = [
    'shopLogin' => '', // Your ShopLogin
    'api_key' => '', //Your Api Key
    'mode' => 'local' // ['production', '*'];
];

$payment = new Payment($data);

$params = [
    'amount' => 50,
    'currency' => Payment::CUR_EURO,
    'shopTransactionID' => '0',
    'itemType' => 'digital',
    'languageID' => Payment::LANG_IT
];

$response = $payment->create($params);

$params = [
    'responseURLs' => [
        'buyerOK' => '',
        'buyerKO' => '',
        'serverNotificationURL' => '',
    ],
    'paymentType' => Payment::PAY_CREDIT_CART,
    'paymentTypeDetails' => [
        'creditcard' => [
            'number' => '4775718800002026', //GestPay demo card
            'expMonth' => '05', //GestPay demo card
            'expYear' => '27', //GestPay demo card
            'CVV' => '123', //GestPay demo card
            'requestToken' => 'MASKEDPAN',
            'DCC' => 'False'
        ]
    ]
];

$response = $payment->submit($params, $response->paymentToken);
$paymentId = $response->paymentId;

$params = [
    'paymentId' => $paymentId
];

$response = $payment->details($params);
var_dump($response);
