<?php

namespace KibokoSrl\GestPayRest\Traits;

use KibokoSrl\GestPayRest\Exceptions\ResponseException;

trait Response
{
    protected function parseResponse($response, $request)
    {
        $response_body = $response->getBody()->getContents();
        if ($response_body != null) {
            $body = is_json($response_body) ? json_decode($response_body) : null;
        } else {
            $body = null;
        }

        if ($response->getStatusCode() == 200) {
            return ($body != null) ? $body->payload : null;
        } else {
            if ($body != null && isset($body->error) && ! empty($body->error)) {
                throw new ResponseException($body->error->description, $body->error->code, $response, $request);
            } else {
                throw new ResponseException('Errore sconosciuto', $response->getStatusCode(), $response, $request);
            }
        }
    }
}
