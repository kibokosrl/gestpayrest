<?php

namespace KibokoSrl\GestPayRest\Classes;

use KibokoSrl\GestPayRest\Request;
use KibokoSrl\GestPayRest\Traits\Response;
use KibokoSrl\GestPayRest\Exceptions\ValidationException;

class Payment extends Request
{
    use Response;

    const LANG_IT = 1;
    const LANG_EN = 2;
    const LANG_SP = 3;
    const LANG_FR = 4;
    const LANG_DE = 5;

    const PAY_CREDIT_CART = 'CREDITCARD';

    const CUR_EURO = 'EUR';

    public function create($paras = [], $headers = [], $withAuth = true)
    {
        $madatory_fields = ['amount', 'currency', 'shopTransactionID'];
        $allowed = array_merge($madatory_fields, [
            '3ds20Container', 'buyerName', 'buyerEmail', 'customInfo', 'paymentChannel', 'requestToken', 'languageId',
            'clientIP', 'itemType', 'recurrent', 'shippingDetails', 'orderDetails'
        ]);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);
        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('payment/create', $validated['data'], $headers, $withAuth);

        return $this->parseResponse($response, $validated);
    }

    public function submit($paras = [], $headers = [], $withAuth = true)
    {
        $madatory_fields = [];
        $allowed = array_merge($madatory_fields, [
            'paymentType', 'buyer.name', 'buyer.email', 'paymentTypeDetails', 'responseURLs'
        ]);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);
        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('payment/submit', $validated['data'], $headers, $withAuth);

        return $this->parseResponse($response, $validated);
    }

    public function details($paras = [], $headers = [], $withAuth = true)
    {
        $madatory_fields = [];
        $allowed = array_merge($madatory_fields, [
            'shopTransactionID', 'bankTransactionID', 'paymentID', 'terminalID'
        ]);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);

        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('payment/detail', $validated['data'], $headers, $withAuth);

        return $this->parseResponse($response, $validated);
    }

    public function refound($paras = [], $headers = [], $withAuth = true)
    {
        $madatory_fields = ['amount', 'currency'];
        $allowed = array_merge($madatory_fields, [
            'bankTransactionID', 'shopTransactionID', 'paymentID', 'termirefundReasonnalID', 'chargeBackFraud', 'orderDetail'
        ]);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);

        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('payment/refund', $validated['data'], $headers, $withAuth);

        return $this->parseResponse($response, $validated);
    }
}
