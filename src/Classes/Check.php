<?php

namespace KibokoSrl\GestPayRest\Classes;

use KibokoSrl\GestPayRest\Request;
use KibokoSrl\GestPayRest\Traits\Response;
use KibokoSrl\GestPayRest\Exceptions\ValidationException;

class Check extends Request
{
    use Response;

    public function cvv($paras = [])
    {
        $madatory_fields = [];
        $allowed = array_merge($madatory_fields, ['creditcard.cvv']);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);
        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('check/cvv', $validated['data']);

        return $this->parseResponse($response, $validated);
    }

    public function expireDate($paras = [])
    {
        $madatory_fields = [];
        $allowed = array_merge($madatory_fields, ['creditcard.expMonth', 'creditcard.expYear']);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);
        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('check/expirydate', $validated['data']);

        return $this->parseResponse($response, $validated);
    }

    public function pan($paras = [])
    {
        $madatory_fields = ['creditcard.number'];
        $allowed = array_merge($madatory_fields, []);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);
        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('check/pan', $validated['data']);

        return $this->parseResponse($response, $validated);
    }

    public function creditCard($paras = [])
    {
        $madatory_fields = ['shopTransactionID'];
        $allowed = array_merge($madatory_fields, ['withAuth', 'creditcard.number', 'creditcard.token', 'expMonth', 'expYear', 'CVV']);

        $validated = $this->validateFields($paras, $madatory_fields, $allowed);
        if ($validated['status'] !== true) {
            throw new ValidationException('Validation error', $validated);
        }

        $response = $this->doPost('check/creditCard', $validated['data']);

        return $this->parseResponse($response, $validated);
    }
}
