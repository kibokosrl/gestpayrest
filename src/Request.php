<?php

namespace KibokoSrl\GestPayRest;

class Request
{
    protected $client;
    protected $config;
    protected $shopLogin;
    protected $headers;

    public function __construct($custom_config = [])
    {
        $default_config = [
            'mode' => 'production',
            'version' => 'v1',
            'api_key' => null,
            'shopLogin' => null,
        ];

        $this->config = array_merge($default_config, $custom_config);

        $this->shopLogin = $this->config['shopLogin'];
        unset($this->config['shopLogin']);

        $this->client = new \GuzzleHttp\Client([
            'base_uri' => baseEndpoint($this->config['mode'], $this->config['version']),
            'timeout' => 90.0
        ]);

        $this->headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'apikey ' . $this->config['api_key']
        ];
    }

    public function setApiKey($api_key)
    {
        $this->headers['Authorization'] = 'apikey "' . $api_key . '"';
    }

    public function validateFields($data, $required, $allowed)
    {
        return [
            'status' => true,
            'data' => $data
        ];
    }

    public function doPost($endpoint, $data = [], $additional_headers = [], $withAuth = true)
    {
        $params = array_merge([
            'shopLogin' => $this->shopLogin
        ], $data);

        $headers = array_merge($this->headers, $additional_headers);

        if ($withAuth === false) {
            unset($headers['Authorization']);
        }

        try {
            return $this->client->post($endpoint, [
                'headers' => $headers,
                'json' => $params
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return $e->getResponse();
        }
    }

    public function doGet($endpoint, $data = [])
    {
        try {
            return $this->client->get($endpoint, $data);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return $e->getResponse();
        }
    }
}
