<?php

function baseEndpoint($mode = 'production', $version = 'v1')
{
    if ($mode == 'production') {
        return 'https://ecomms2s.sella.it/api/' . $version . '/';
    } else {
        return 'https://sandbox.gestpay.net/api/' . $version . '/';
    }
}

function is_json($string)
{
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}
