<?php

namespace KibokoSrl\GestPayRest\Exceptions;

class ValidationException extends \Exception
{
    protected $response;

    public function __construct($message, $response = null)
    {
        $this->response = $response;
        parent::__construct($message);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
