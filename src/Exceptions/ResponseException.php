<?php

namespace KibokoSrl\GestPayRest\Exceptions;

class ResponseException extends \Exception
{
    protected $response;
    protected $request;

    public function __construct($message, $code, $response = null, $request = null)
    {
        $this->response = $response;
        $this->request = $request;

        parent::__construct($message, $code);
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getRequest()
    {
        return $this->request;
    }
}
